//
//  ViewController.swift
//  TrabalhoAvaliativo
//
//  Created by COTEMIG on 20/10/22.
//

import UIKit
import Kingfisher
import Alamofire

struct personagem: Decodable {
    
    let image: String
    let name: String
    let actor: String
   
}

class ViewController: UIViewController, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listaPersonagem.count
    }
    
    
    @IBOutlet weak var tableView: UITableView!
    
    var listaPersonagem: [personagem] = []
    var lista:[personagem] = []
    var userDefaults = UserDefaults.standard
    var listaKey = "lista"
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let celula = tableView.dequeueReusableCell(withIdentifier: "MinhaCelula", for: indexPath) as! MyCell
        let list = listaPersonagem[indexPath.row]
        
        celula.nomeAtor.text = list.name
        celula.nomePersonagem.text = list.actor
        celula.imagem?.kf.setImage(with: URL(string: list.image))
        
        return celula
        
    }
    
    func novoPersonagem(){
        AF.request("https://hp-api.herokuapp.com/api/characters").responseDecodable(of: [personagem].self) { response in if let personagem = response.value {
                self.listaPersonagem = personagem
            }
            self.tableView.reloadData()
        }
    
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        novoPersonagem()
    }
        
}
